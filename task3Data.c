//
// Created by ben on 26/2/2020.
//

#include <stdio.h>
#include <pthread.h>
#include <malloc.h>

typedef struct taskThreadData {
    char* message;
    int value;
} task_thread_data;

void *DataThread(void *arg) {
    task_thread_data *ttdata;
    ttdata = (task_thread_data*) arg;
    for (int i=0; i<ttdata->value; i++) {
        printf("hello world. Msg: [%s]\n", ttdata->message);
    }
    pthread_exit(NULL);
}

int main (void) {
    task_thread_data* ttdata;
    ttdata = (task_thread_data *) malloc(sizeof(task_thread_data));
    ttdata->message = "Print this message";
    ttdata->value = 5;  // Print it this many times

    pthread_t helloThread;
    int returnval = pthread_create(&helloThread, NULL, DataThread, ttdata);

    pthread_join(helloThread,NULL);

    return 0;
}