//
// Created by ben on 26/2/2020.
//

#include <stdio.h>
#include <pthread.h>
#include <malloc.h>
#include <stdlib.h>

typedef struct taskThreadData {
    int tId;
    int msgRepeat;
} task_thread_data;

void *RepeaterThread(void *arg) {
    task_thread_data *ttdata;
    ttdata = (task_thread_data*) arg;
    for (int i=0; i<ttdata->msgRepeat; i++) {
        printf("tId [%d]. Printing [%d] of [%d]\r\n", ttdata->tId,i,ttdata->msgRepeat);
    }
    pthread_exit(NULL);
}

int main (void) {
    srand((unsigned int) time(0));
    int numberThreads = 1 + (rand()%20);
    printf ("Starting %d random threads\r\n",numberThreads);
    for (int i = 0; i < numberThreads; i++) {
        task_thread_data *ttdata;
        ttdata = (task_thread_data *) malloc(sizeof(task_thread_data));
        ttdata->tId = 1 + (rand() %1000);
        ttdata->msgRepeat = 1 + (rand()%20);

        printf("Building thread %d with %d repeats",ttdata->tId,ttdata->msgRepeat);

        pthread_t helloThread;
        int returnval = pthread_create(&helloThread, NULL, RepeaterThread, ttdata);

        if (returnval != 0) {   // Check status
            printf("Error #[%d]occurred starting thread with id %d.\r\n",returnval,ttdata->tId);
        } else {    // Valid thread - join it
            pthread_join(helloThread, NULL);
        }
    }

    return 0;
}