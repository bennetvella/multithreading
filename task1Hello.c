//
// Created by ben on 26/2/2020.
//

#include <stdio.h>
#include <pthread.h>

void *HelloThread(void *arg) {
    printf ("hello world");
    pthread_exit(NULL);
}

int main (void) {
    pthread_t helloThread;

    int returnval = pthread_create(&helloThread, NULL, HelloThread, "1");

    pthread_join(helloThread,NULL);
    return 0;
}