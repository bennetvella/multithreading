//
// Created by ben on 25/2/2020.
//

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

typedef struct thread_data {
    long threadId;
    char* message;
} thread_data_t;

void *MyThread(void *arg){
    thread_data_t *data;
    data = (thread_data_t*) arg;
    for (int i = 0; i<5; i++) {
        printf("From inside the created thread %ld with message [%s]\n", data->threadId, data->message);
        sleep(1);
    }
    pthread_exit(NULL);
}

void thread_cleanup(void *arg) {
    printf ("Cleanup running\n");
}

void *ThreadThatSleeps(void *arg) {
    pthread_cleanup_push(thread_cleanup, NULL);
    //pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
    sleep(10);
    printf("I slept fully\n");
    pthread_exit(NULL);
            pthread_cleanup_pop(1); // call clean up function if arg > 0
}

int main(void) {
    int returnval;
    pthread_t thread;
    pthread_t threadSleep;

    thread_data_t *tdata;
    tdata = (thread_data_t *) malloc(sizeof(thread_data_t));
    tdata->threadId = 456;
    tdata->message = "Test";

    returnval = pthread_create(&thread,NULL, MyThread, (void *)tdata);
    returnval = pthread_create(&threadSleep,NULL,ThreadThatSleeps,NULL);

    printf("Main Thread done\n");

    //pthread_detach(threadSleep);
    pthread_join(thread, NULL);
    pthread_cancel(threadSleep);
    pthread_join(threadSleep, NULL);

    printf("Threads have finished\n");
    return 0;
}