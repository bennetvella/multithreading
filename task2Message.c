//
// Created by ben on 26/2/2020.
//

#include <stdio.h>
#include <string.h>
#include <pthread.h>

void *MessageThread(void *arg) {
    char *msg_ptr = (char*)arg;
    printf ("hello world. Msg: [%s]", msg_ptr);
    pthread_exit(NULL);
}

int main (void) {
    pthread_t helloThread;
    int returnval = pthread_create(&helloThread, NULL, MessageThread, "This Message");

    pthread_join(helloThread,NULL);
    return 0;
}