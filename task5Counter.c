//
// Created by ben on 26/2/2020.
//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

// This atomic var still doesn't achieve synchronization.
// It probably needs to be implemented in a different way.
__sig_atomic_t counter = 0;

void *CounterThread(void *arg) {
    int tId = (int) arg;
    for (int i=0; i < 10000000; i++) {
        counter ++;
        if (counter % 1000 == 0) {
            printf("Thread %d reached %d\r\n", tId, counter);
        }
    }
    pthread_exit(NULL);
}

int main (void) {
    printf ("Starting 10 incrementing threads\r\n");
    pthread_t countingThreads[10];
    for (int i = 0; i < 10; i++) {
        int returnval = pthread_create(&countingThreads[i], NULL, CounterThread, (void *) i);

        if (returnval != 0) {   // Check status
            printf("Thread creation error #[%d] occurred\r\n",returnval);
        }
    }

    for (int i = 0; i < 10; ++i) {
        pthread_join(countingThreads[i], NULL);
    }

    printf ("Finish with counter: %d",counter);
    return 0;
}